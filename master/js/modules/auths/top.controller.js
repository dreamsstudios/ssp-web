/**=========================================================
 * Module: top.controller.js
 * Demo for TopController api
 =========================================================*/
(function() {
    'use strict';
    angular
        .module('app.auths')
        .controller('TopFormController', TopFormController);

    TopFormController.$inject = ['$rootScope', '$scope', '$http', '$state'];

    function TopFormController($rootScope, $scope, $http, $state) {
        var vm = this;
        activate();
        function activate() {
            vm.getdetails = function() {
				$state.go('app.stock');
            };
			vm.shorting = function(ev) {
				$state.go('app.dashboard');
				if(ev!='more'){
					angular.element('.home_graph').find('ul li.stockli').hide();
					for(var i=0;i<ev;i++)
						 angular.element('.home_graph').find('ul').find('li.stockli:eq('+i+')').show();
				}
				else
					angular.element('.home_graph').find('ul li.stockli').show();
            };
        }
    }
})();